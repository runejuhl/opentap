# FAQ

## What does OpenTAP mean?
OpenTAP stands for: Open (source) Test Automation Project

## What is OpenTAP?
An open source test sequencing engine.

## Who can use OpenTAP?
The project is available online at [http://Gitlab.com/OpenTAP/OpenTAP](http://Gitlab.com/OpenTAP/OpenTAP) and available to anyone in the world to contribute, collaborate and create with.

## How do I take advantage of OpenTAP?
Please refer to the documentation for full details.

## Who is responsible for OpenTAP?
See our about page for details at [https://www.opentap.io/about.html](https://www.opentap.io/about.html).

## Am I allowed to take the code and do anything I like with it?
Code is made available under the MPL v.2 license allow users to take the code and modify it as long as it is kept open source under the MPL v.2 license.

## What is the MPL v.2 Open Source License?
The license used for OpenTAP is MPL v.2 which is a weak copyleft license where the source code needs to stay open and if modifications are made to licensed part of the code then those need to be open source under the MPL v.2 as well. You can read about different types of open source licenses and their implications here.

## Where can I find the source code?
The code is hosted at [http://Gitlab.com/OpenTAP/OpenTAP](http://Gitlab.com/OpenTAP/OpenTAP).

## What is a Contributor License Agreement (CLA)?
A CLA ensures that the project retains full copyright of the codebase. To retain the copyright, users wanting to contribute with their own code, ideas or issue fixes will need to sign their copyright over to Keysight Technologies, the project originator, using this CLA.

## How do I get support for OpenTAP?
As an open source project, good faith support is provided to community members through the project destination itself on [http://Gitlab.com/OpenTAP/OpenTAP](http://Gitlab.com/OpenTAP/OpenTAP). In addition, we monitor a support email node for questions affecting the open source project.